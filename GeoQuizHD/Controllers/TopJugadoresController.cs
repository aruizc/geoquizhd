﻿using GeoQuizHD.Models;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;

namespace GeoQuizHD.Controllers
{
    public class TopJugadoresController : Controller
    {
        GeoQuizHDEntities bd = new GeoQuizHDEntities();

        // GET: TopJugadores
        public ActionResult Index()
        {
            return View((Usuario)Session["Usuario"]);
        }
        public ActionResult ObtenerTop([DataSourceRequest]DataSourceRequest request)
        {
            try
            {
                List<Usuario> usuarios = bd.Usuario.OrderByDescending(u => u.Puntaje).Take(10).ToList();
                DataSourceResult result = usuarios.ToDataSourceResult(request, u => new
                {
                    IdUsuario = u.IdUsuario,
                    Puntaje = u.Puntaje,
                    Nombre = u.Nombre,
                    Paterno = u.Paterno,
                    Materno = u.Materno,
                    Correo = u.Correo,
                    Nickname = u.Nickname

                });
                return Json(result);
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}