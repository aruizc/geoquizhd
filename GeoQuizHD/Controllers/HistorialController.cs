﻿using GeoQuizHD.Models;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;

namespace GeoQuizHD.Controllers
{
    public class HistorialController : Controller
    {

        GeoQuizHDEntities bd = new GeoQuizHDEntities();
        // GET: Historial
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ObtenerHistorial([DataSourceRequest]DataSourceRequest request)
        {
            try
            {
                Usuario usuario = (Usuario)Session["Usuario"];
                List<Partida> partidas = bd.Partida.OrderByDescending(u => u.Fecha).Where(p => p.IdJugador == usuario.IdUsuario).ToList();
                DataSourceResult result = partidas.ToDataSourceResult(request, p => new
                {
                    IdJugador = p.IdJugador,
                    Puntaje = p.Puntaje,
                    Fecha = p.Fecha,
                    Turnos = p.Turnos                  

                });
                return Json(result);
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}