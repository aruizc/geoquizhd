﻿using GeoQuizHD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GeoQuizHD.Controllers
{
    public class JuegoController : Controller
    {
        GeoQuizHDEntities bd = new GeoQuizHDEntities();

        // GET: Juego
        public ActionResult Index()
        {
            Usuario usuario = (Usuario)Session["Usuario"];
            if (usuario != null)
                return View();
            else
                return null;
        }

        public ActionResult GuardarPartida(int turno, int puntaje)
        {
            bool valido = false;
            try
            {
                Partida partida = new Partida();
                Usuario usuario = (Usuario)Session["Usuario"];
                usuario.Puntaje += puntaje;
                partida.IdJugador = usuario.IdUsuario;
                partida.Fecha = DateTime.Now;
                partida.Puntaje = puntaje;
                partida.Turnos = turno;
                bd.Partida.Add(partida);
                bd.Entry(usuario).State = System.Data.Entity.EntityState.Modified;
                bd.SaveChanges();
                valido = true;
            }
            catch (Exception ex)
            {
                throw;
            }
            return Json(new { result = valido });
        
        }

        public ActionResult MuestraPregunta(string pais)
        {
            Pregunta pregunta = new Pregunta();
            try
            {
                switch (pais)
	            {
                    case "china":
                        pais = "China";
                        break;
                     case "rusia":
                        pais = "Rusia";
                        break;
                     case "singapur":
                        pais = "Singapur";
                        break;
                     case "coreasur":
                        pais = "Corea del Sur";
                        break;
                     case "israel":
                        pais = "Israel";
                        break;
                     case "india":
                        pais = "India";
                        break;
                    case "arabia":
                        pais = "Arabia Saudita";
                        break;
	            }
                Random random = new Random();
                List<Pregunta> lstPreguntas = bd.Pregunta.ToList();
                int randomNumber = random.Next(0, lstPreguntas.Count);
                pregunta = lstPreguntas[randomNumber];
              
            }
            catch (Exception ex)
            {
                
            }
            return Json(pregunta , JsonRequestBehavior.AllowGet);
        }
    }
}