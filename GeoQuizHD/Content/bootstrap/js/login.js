﻿$(function () {

    $('#login-form-link').click(function (e) {
        $("#formLogin").delay(100).fadeIn(100);
        $("#formRegistrar").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#register-form-link').click(function (e) {
        $("#formRegistrar").delay(100).fadeIn(100);
        $("#formLogin").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });

});